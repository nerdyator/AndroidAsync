package chat.nirmalit.com.androidasync.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import chat.nirmalit.com.androidasync.RoomDB.AppDatabase;
import chat.nirmalit.com.androidasync.other.CommonUtils;
import chat.nirmalit.com.androidasync.other.Message;

/**
 * Created by nerdyator on 12/03/18.
 */

public class ChatService extends Service {
    private final IBinder iBinder = new ChatBinder();
    private final String TAG="Chat";
    private AsyncHttpClient asyncHttpClient;
    private AppDatabase db;
    private Message m = null;
    private Future<WebSocket> webSocketFuture=null;
    final public static String MY_ACTION = "MY_ACTION";
    private String token = null;
    public ArrayList<Message> listMessages;
    private int uid;
    private SharedPreferences sharedPreferences;

    public class ChatBinder extends Binder{
        public ChatService getService(){
            Log.d(TAG,"get service called");
            return ChatService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG,"On Create command Called");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG,"On start command Called");
        //call to websocket connect to send a ping as it called on service creation.
        asyncHttpClient = AsyncHttpClient.getDefaultInstance();
        db = AppDatabase.getAppDatabase(this);

        token = intent.getStringExtra("token");

        if(webSocketFuture==null){
            Log.d(TAG,"Websocketfuture is null");
            webSocketFuture = asyncHttpClient.websocket("ws://mit.edufily.com:8001/chat/?token=" + token, null, new AsyncHttpClient.WebSocketConnectCallback() {
                @Override
                public void onCompleted(Exception ex, WebSocket webSocket) {
                    if(ex==null){
                        Log.d(TAG,"handshake completed successfully");

                        sendMessage(prepareMsgToSend("ping",1,0));
                    }else {
                        Log.d(TAG,"Got an error while connecting");
                        ex.printStackTrace();
                    }
                }

            });

        }
            return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"onBind Called");
        //sendMessage(m);
        return iBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onCreate();
    }

    public Message prepareMsgToSend(String text, int gid, int type)
    {
        sharedPreferences = getApplicationContext().getSharedPreferences("User", Context.MODE_PRIVATE);
        uid = Integer.parseInt(sharedPreferences.getString("user_id", "-1"));

        long mk = new Date().getTime();
        long new_mk = Long.valueOf(String.valueOf(uid)+String.valueOf(mk));
        m = new Message("",text,null, uid, new_mk,gid,type);
        return m;
    }

    public ArrayList<Message> getMessages(){
        listMessages = (ArrayList<Message>)db.messageDAO().getAllSort();
        return listMessages;
    }


    private void playBeep(){
        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void sendMessage(final Message m){
        uid = Integer.parseInt(sharedPreferences.getString("user_id", "-1"));

        Log.d(TAG, m.toString());
        Log.d(TAG,"Websocketfuture is not null");
        final Gson g = new Gson();
            final WebSocket webSocket = webSocketFuture.tryGet();
            if(webSocket!=null){
                Log.d(TAG,"Even Websocket is not null");
                Log.d(TAG, g.toJson(m));
                webSocket.send(g.toJson(m));
                webSocket.setStringCallback(new WebSocket.StringCallback() {
                    @Override
                    public void onStringAvailable(String s) {
                        Log.d(TAG, "Receive msg in websocket callback: "+s);
                        listMessages = (ArrayList<Message>) db.messageDAO().getAll();
                        Message recMsg = g.fromJson(s, Message.class);
                        Log.d(TAG,"Receive Msg: "+ recMsg.toString());
                        if(recMsg.getType()==1){
                            Log.d(TAG, "Recevied message type: "+String.valueOf(m.getType()));
                            try{
                                JSONObject j = new JSONObject();
                                j.put("umi",recMsg.getUmi());
                                j.put("type",2);
                                webSocket.send(j.toString());
                                Log.d(TAG, j.toString());
                                Log.d(TAG,"Acknowledgement sent for: "+recMsg.toString());
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                            recMsg.setTime(CommonUtils.getTimeStr(recMsg.getT()));
                            if (recMsg.getSi() == uid) {
                                Log.d(TAG, "Sender id and local id matched so updating the local message");
                                        db.messageDAO().updateMessage(recMsg);
                            } else {
                                Log.d(TAG, "Sender id and local id didn't matched so adding message to db");
                                db.messageDAO().insertAll(recMsg);
                                playBeep();
                            }
                            Intent intent = new Intent();
                            intent.setAction(MY_ACTION);
                            intent.putExtra("message", "message");
                            sendBroadcast(intent);
                        }else{
                            Log.d(TAG, "Message type is Ping-Pong");
                        }
                    }
            });
            }else{
                Log.d(TAG, " new WebSocket is null");
            }

    }
}
