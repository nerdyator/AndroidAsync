package chat.nirmalit.com.androidasync.other;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chat.nirmalit.com.androidasync.R;


/**
 * Created by abhilash on 17/2/17.
 *
 * @author abhilash
 */
public class CommonUtils {

    public static final String DATA_DIR = "Voicera";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //public static final String EMAIL_PATTERN = "[a-z0-9._-]+@[a-z0-9.-]+\\.[a-z]{2,4}";


    public static boolean isExtStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static void createDataDirIfNotExists() {
        if (isExtStorageAvailable()) {
            File root = Environment.getExternalStorageDirectory();
            File dataDir = new File(root, DATA_DIR);
            if (!(dataDir.exists() && dataDir.isDirectory())) {
                dataDir.mkdirs();
            }
        }
    }

    public static boolean validateURL(String url) {
        return Patterns.WEB_URL.matcher(url).matches();
    }

    public static String getDataDir() {
        createDataDirIfNotExists();
        File dataDir = new File(Environment.getExternalStorageDirectory(), DATA_DIR);
        return dataDir.getAbsolutePath();
    }

   /* public static void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) App.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideKeyboard(EditText editText) {
        try {
            InputMethodManager imm = (InputMethodManager) App.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }*/

    public static void hideKeyboard(Activity activity, EditText editText) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }


   /* public static void showKeyboard(EditText editText, boolean isForced) {
        try {
            InputMethodManager imm = (InputMethodManager) App.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (isForced) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                editText.requestFocus();
            } else {
                imm.showSoftInputFromInputMethod(editText.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }*/

    public static boolean validateEmail(String email) {
        Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = emailPattern.matcher(email);
        return matcher.matches();
    }

    /*public static void showKeyboard(EditText editText) {
        showKeyboard(editText, false);
    }*/

    public static SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    public static SimpleDateFormat dateFormatForEvent = new SimpleDateFormat("hh:mm a", Locale.getDefault());

    public static SimpleDateFormat dateFormatForDay = new SimpleDateFormat("dd MMM yyyy hh:mm", Locale.getDefault());

   /* public static Dialog getProgressDialog(Activity activity, int titleText, boolean cancelableFlag) {
        if (titleText == 0)
            titleText = R.string.please_wait;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressbar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(cancelableFlag);
        TextView title = ButterKnife.findById(dialog, R.id.tvLoaderText);
        title.setText(activity.getResources().getString(titleText));
        return dialog;
    }*/



	/*public static Dialog getProgressDialog(Activity activity) {
		Dialog dialog = new Dialog(activity);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog_progress);
		return dialog;
	}*/


    public static String getFontName(int index) {
        String font = "Regular.ttf";
        switch (index) {
            case 2:
                font = "Bold.ttf";
                break;
            case 3:
                font = "Light.ttf";
                break;
        }
        return font;
    }

   /* public static int getDpToPixel(int dp) {
        float scale = App.getAppContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }*/

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static void copyFile(File src, File dest) throws IOException, NullPointerException {
        if (null == src || null == dest) {
            throw new NullPointerException("Source or Destination is null");
        }
        File parentFile = dest.getParentFile();
        if (parentFile != null) {
            if (!parentFile.mkdirs() && !parentFile.isDirectory()) {
                throw new IOException("Destination directory cannot be created");
            }
        }
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        sourceChannel = new FileInputStream(src).getChannel();
        destChannel = new FileOutputStream(dest).getChannel();
        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        sourceChannel.close();
        destChannel.close();
    }

    public static String getFormattedDateForDeadLine(Date date) {
        String dateFormatted = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            dateFormatted = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateFormatted;
    }


	/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

//    public static Dialog getProgressDialog(Activity activity) {
//        Dialog dialog = new Dialog(activity);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setContentView(R.layout.dialog_progress);
//        return dialog;
//    }

    public static long getDate(String time) {
        //2017-10-15T07:31:32.189753
        if (!StringUtils.isEmpty(time)) {
            try {
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.ENGLISH);
                return (sdfServer.parse(time).getTime() / 1000);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return (System.currentTimeMillis() / 1000);
    }

    public static String getMonthString(String date) {
        return getMonthId(Integer.parseInt(getMonth(date)));
    }

    private static String getMonth(String date) {
        if (!StringUtils.isEmpty(date)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM", Locale.ENGLISH);
                //2017-10-25T11:00:30
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                return sdf.format(sdfServer.parse(date).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getDateString(String date) {
        if (!StringUtils.isEmpty(date)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd", Locale.ENGLISH);
                //2017-10-25T11:00:30
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                return sdf.format(sdfServer.parse(date).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getYearString(String date) {
        if (!StringUtils.isEmpty(date)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy", Locale.ENGLISH);
                //2017-10-25T11:00:30
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                return sdf.format(sdfServer.parse(date).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }


    private static String getMonthId(int month) {
        switch (month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "";
        }
    }

   /* public static int getFileType(String type) {
        if (!StringUtils.isEmpty(type)) {
            switch (type) {
                case "AUDIO":
                    return R.mipmap.ic_audio;
                case "EXCEL":
                    return R.mipmap.ic_xls;
                case "IMAGE":
                    return R.mipmap.ic_image;
                case "PDF":
                    return R.mipmap.ic_pdf;
                case "POWERPOINT":
                    return R.mipmap.ic_ppt;
                case "TEXT":
                    return R.mipmap.ic_text;

                case "VIDEO":
                    return R.mipmap.ic_video;
                case "WORD":
                    return R.mipmap.ic_doc;
                case "ZIP":
                    return R.mipmap.ic_zip;
                default:
                    return R.mipmap.ic_file;
            }
        } else {
            return R.mipmap.ic_file;
        }
    }*/

    public static String getDateStr(String date) {
        if (!StringUtils.isEmpty(date)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                //2017-10-25T11:00:30
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                Log.d("Chat", sdf.format((sdfServer.parse(date).getTime())).toString());
                return sdf.format(sdfServer.parse(date).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getTimeStr(String date) {
        if (!StringUtils.isEmpty(date)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
                //2017-10-25T11:00:30
                SimpleDateFormat sdfServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                Log.d("Chat", sdf.format((sdfServer.parse(date).getTime())).toString());
                return sdf.format(sdfServer.parse(date).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
