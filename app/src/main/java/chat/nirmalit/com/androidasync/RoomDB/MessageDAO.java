package chat.nirmalit.com.androidasync.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import chat.nirmalit.com.androidasync.other.Message;

/**
 * Created by nerdyator on 28/02/18.
 */

@Dao
public interface MessageDAO {

    @Query("Select * from tbl_messages")
    List<Message> getAll();

    @Query("Select * from tbl_messages ORDER BY msg_time")
    List<Message> getAllSort();

    @Query("select count(*) from tbl_messages")
    int getCount();

    @Insert
    void insertAll(Message... messages);

    @Delete
    void delete(Message message);

    @Update
    int updateMessage(Message messaage);

}
