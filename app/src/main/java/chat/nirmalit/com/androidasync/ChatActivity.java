package chat.nirmalit.com.androidasync;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import chat.nirmalit.com.androidasync.RoomDB.AppDatabase;
import chat.nirmalit.com.androidasync.Services.ChatService;

import chat.nirmalit.com.androidasync.other.Message;
import chat.nirmalit.com.androidasync.other.Utlis;

public class ChatActivity extends AppCompatActivity implements ServiceConnection{

    private Button sendBtn;
    private EditText inputMsg;
    private ListView listViewMessges;
    private List<Message> listMessages;
    private MessageListAdapter adapter;
    private AppDatabase db;
    private final String TAG = "Chat";
    private ChatService chatService;
    private boolean mBound = false;
    private boolean isMessageReceiverRegistered =false;
    MessageReceiver messageReceiver = null;

    private Utlis utils;

    private int uid;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        db = AppDatabase.getAppDatabase(getApplicationContext());
        sendBtn = (Button) findViewById(R.id.btnSend);
        inputMsg = (EditText)findViewById(R.id.inputMsg);
        listViewMessges = (ListView)findViewById(R.id.list_view_messages);

        utils = new Utlis(getApplicationContext());

        listMessages = new ArrayList<Message>();
        adapter = new MessageListAdapter(this, listMessages);
        listViewMessges.setAdapter(adapter);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("User", Context.MODE_PRIVATE);
        final int uid = Integer.parseInt(sharedPreferences.getString("user_id","-1"));
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Date date = new Date();
                final String message = inputMsg.getText().toString().trim();
                final long message_key = (long) date.getTime();
                Message m = chatService.prepareMsgToSend(message,3,1);
                //my message so add it to db first
                appendMessage(m);
                inputMsg.setText(null);

                //then send it to socket to send it to server
                chatService.sendMessage(m);

            }
        });

    }

    @Override
    protected  void onStart(){
        super.onStart();
        Intent intent = new Intent(this, ChatService.class);
        bindService(intent,this, Context.BIND_AUTO_CREATE);
        mBound=true;
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(isMessageReceiverRegistered){
            unregisterReceiver(messageReceiver);
            messageReceiver = null;
            isMessageReceiverRegistered=false;
        }
        if(mBound){
            unbindService(this);
            mBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isMessageReceiverRegistered && messageReceiver == null){
            messageReceiver = new MessageReceiver();
            registerReceiver(messageReceiver,new IntentFilter(ChatService.MY_ACTION));
            isMessageReceiverRegistered = true;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private void appendMessage(final Message m) {
        if(m.getT()==null&&m.getType()==1) {
            Log.d(TAG,"message Time is null so showing to local side");
            db.messageDAO().insertAll(m);
            listMessages.add(m);
        }else{
            playBeep();
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
//        if (m.getType() == 1) {
//            if (m.getSi() == uid) {
//                Log.d(TAG, "Sender id and local id matched so updating the local message");
//                for (Message m1 : listMessages) {
//                    if (m1.getMk() == m.getMk()) {
//                        m1.setT(m.getT());
//                        db.messageDAO().updateMessage(m);
//                    }
//                }
//            } else {
//                Log.d(TAG, "Sender id and local id didn't matched so adding message to db");
//                    db.messageDAO().insertAll(m);
//                    listMessages.add(m);
//                }
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    inputMsg.setText("");
//                    adapter.notifyDataSetChanged();
//
//                }
//            });
//        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d(TAG,"onServiceCompleted called");
        ChatService.ChatBinder chatBinder = (ChatService.ChatBinder)iBinder;
        chatService = chatBinder.getService();
        mBound=true;
        listMessages.addAll(chatService.getMessages());

    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Log.d(TAG,"OnServiceDisconnected called");
        mBound=false;
    }

    private void playBeep(){
        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"In Broadcast Receiver");
            listMessages.clear();
            listMessages.addAll(db.messageDAO().getAllSort());
            adapter.notifyDataSetChanged();
            //playBeep();

        }
    }

}
