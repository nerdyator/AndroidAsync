package chat.nirmalit.com.androidasync;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import chat.nirmalit.com.androidasync.other.Message;

/**
 * Created by nerdyator on 12/4/17.
 */

public class MessageListAdapter extends BaseAdapter {
    private Context context;
    private List<Message> messagesItems;

    public MessageListAdapter(Context context, List<Message> navDrawerItems) {
        this.context = context;
        this.messagesItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        Message m = messagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        SharedPreferences sharedPreferences = context.getSharedPreferences("User",Context.MODE_PRIVATE);
        int uid = Integer.parseInt(sharedPreferences.getString("user_id","-1"));

        //System.out.println("USER iD from SP:"+ sharedPreferences.getString("user_id","-1"));


        String msgUserName ="";
        int imageID = 0;
        // Identifying the message owner
        if (messagesItems.get(position).getSi()==uid) {
            // message belongs to you, so load the right aligned layout
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_message_right, parent, false);//mInflater.inflate(R.layout.list_item_message_right, null);
            imageID = R.drawable.ic_stopwatch;
            //check whether message doesn't have message_time
            if(!(messagesItems.get(position).getT()==null)){
                imageID = R.drawable.ic_success;
            }
            ImageView imageView = (ImageView)convertView.findViewById(R.id.imgIcon);
            imageView.setImageResource(imageID);


        } else {
            // message belongs to other person, load the left aligned layout
            convertView = mInflater.inflate(R.layout.list_item_message_left,
                    null);
            msgUserName = m.getUn();
        }
        TextView lblFrom = (TextView) convertView.findViewById(R.id.lblMsgFrom);
        TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);
        TextView lblTime = (TextView) convertView.findViewById(R.id.lblMsgTime);
        txtMsg.setText(m.getText());
        lblFrom.setText(msgUserName);
        lblTime.setText(m.getTime());

        return convertView;
    }
}
