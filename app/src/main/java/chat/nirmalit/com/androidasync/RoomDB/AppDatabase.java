package chat.nirmalit.com.androidasync.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import chat.nirmalit.com.androidasync.other.Message;

/**
 * Created by nerdyator on 28/02/18.
 */
@Database(entities = {Message.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase INSTANCE;

    public abstract MessageDAO messageDAO();

    public static AppDatabase getAppDatabase(Context context){
        if(INSTANCE==null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,"message-db")
                    .allowMainThreadQueries().build();
        }
        return INSTANCE;

    }

    public static void destroyDatabase(){
        INSTANCE = null;
    }
}
