package chat.nirmalit.com.androidasync;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpRequest;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONException;
import org.json.JSONObject;

import chat.nirmalit.com.androidasync.RoomDB.AppDatabase;
import chat.nirmalit.com.androidasync.Services.ChatService;

public class MainActivity extends AppCompatActivity {

    private AppDatabase db;
    private Button submitButton;
    private EditText editText;
    private TextView msgCount;
    SharedPreferences sharedPref;
    private static final String KEY_USER_ID = "user_id";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        submitButton = (Button) findViewById(R.id.btnSend);
        editText = (EditText) findViewById(R.id.inputMsg);
        msgCount = (TextView)findViewById(R.id.msgCount);
        db = AppDatabase.getAppDatabase(getApplicationContext());
        msgCount.setText(msgCount.getText()+ String.valueOf(db.messageDAO().getCount()));

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String token = editText.getText().toString().trim();
                final String path = "ws://mit.edufily.com:8001/chat/?token="+token;
//                AsyncHttpClient.getDefaultInstance().websocket(path, null, new AsyncHttpClient.WebSocketConnectCallback() {
//                    @Override
//                    public void onCompleted(Exception ex, WebSocket webSocket) {
//                        if(ex != null ){
//                            System.out.println("Got an Error");
//                            ex.printStackTrace();
//                            return;
//                        }
//                    }
//                });
                Intent service = new Intent(getApplicationContext(), ChatService.class);
                service.putExtra("token", token);
                startService(service);
                JWT jwt = new JWT(token);
                Claim claim = jwt.getClaim("user_id");
                System.out.println(claim.asString());
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("User", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(KEY_USER_ID, claim.asString());
                editor.apply();
                Intent i = new Intent(MainActivity.this, ChatActivity.class);
                i.putExtra("path", path);
                i.putExtra("token", token);
                startActivity(i);
            }
        });
    }
}
