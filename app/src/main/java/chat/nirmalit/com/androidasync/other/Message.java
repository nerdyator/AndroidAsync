package chat.nirmalit.com.androidasync.other;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by nerdyator on 12/4/17.
 */
@Entity(tableName = "tbl_messages")
public class Message implements Serializable{

    @ColumnInfo(name="username")
    private String un;
    @ColumnInfo(name="text")
    private String text;
    @ColumnInfo(name="msg_time")
    private String t;
    @ColumnInfo(name="sender_id")
    private int si;
    @PrimaryKey(autoGenerate = false)
    private long mk;
    @ColumnInfo(name="groud_id")
    private int gid;
    @ColumnInfo(name="type")
    private int type;
    @ColumnInfo(name="unique_msg_id")
    private int umi;
    private String time;


    public Message(String un, String message, String message_time, int sender_id, long message_key, int group_id, int type, int umi) {
        this.un = un;
        this.text = message;
        this.t = message_time;
        this.si = sender_id;
        this.mk = message_key;
        this.gid = group_id;
        this.type=type;
        this.umi = umi;
    }

    public Message(String un, String message, String message_time, int sender_id, long message_key, int group_id, int type) {
        this.un = un;
        this.text = message;
        this.t = message_time;
        this.si = sender_id;
        this.mk = message_key;
        this.gid = group_id;
        this.type=type;
    }

    public Message() {}

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public long getMk() {
        return mk;
    }

    public void setMk(long mk) {
        this.mk = mk;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getT() { return t; }

    public void setT(String t) {
        this.t = t;
    }

    public int getSi() {
        return si;
    }

    public void setSi(int si) {
        this.si = si;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUmi() {
        return umi;
    }

    public void setUmi(int umi) {
        this.umi = umi;
    }

    @Override
    public String toString() { return "message Key: "+mk +", Message Text: "+ text +", Message Time: "+t+" Message Type: "+type+" umi: "+umi;}
}
